#include <iostream>
#include <libpq-fe.h>
using namespace std;

int main()
{
  PGPing code_retour_ping;
  code_retour_ping = PQping("host=postgresql.bts-malraux72.net port=5432");

  if(code_retour_ping == PQPING_OK)
  {
    cout << "La connexion au serveur de base de données a été établie ave succès" << endl;
    PGconn *connexion;
    connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=a.bouhenni user=a.bouhenni");
    ConnStatusType statut_connexion = PQstatus(connexion);

    if(statut_connexion == CONNECTION_OK)
    {
      cout << "connexion établie avec l'utilisateur " << PQuser(connexion) << endl;
      cout << PQhost(connexion) << endl;
      cout << PQport(connexion) << endl;
      cout << PQdb(connexion) << endl;
      // TODO: afficher une étoile pour chacun des caractères
      cout << PQpass(connexion) << endl;
      cout << PQsslInUse(connexion) << endl;
      cout << PQprotocolVersion(connexion) << endl;
      cout << "version du serveur: " << PQserverVersion(connexion) << endl;
      cout << "version de la bibliotheque libpq du client: " << PQlibVersion() << endl;
    }
    else if(statut_connexion == CONNECTION_BAD)
    {
      cout << "connexion échoué" << endl;
    }
  }
  else
  {
    cerr << "Malheureusement le serveur n’est pas joignable.Vérifier la connectivité" << endl;
  }

  return 0;
}
